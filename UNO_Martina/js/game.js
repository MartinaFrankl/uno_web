$(document).ready(function() {
// Globale Var

// merken uns die spiel id
let game

let players = {} // Mapping zwischen SpielerInnen Name und ihrem div
let playerOrder // SpielerInnenamen in ursprünglicher Reihenfolge
let currentPlayer // speichert aktive/n Spieler/in

// true: aktion kann ausgeführt werdne
// false: kann keine aktion ausführen da noch andere aktiv ist
let playable = false


// oft verwendete div referenz
let $topCard = $('#topCard')
let $draw = $('#draw')
let $colorPicker = $('#colorPicker')

let direction = 1 // fuer spielerInnenwechsel entweder +1/-1

// karten die farbwaehler benoetigen
let colorPickerCards = [13, 14]
// skip player + karten
let skipCards = [10, 13]
// umgekehrte spielerInnen reihenfolge
let reverseCards = [12]

// Mapping zwischen Kartenfarben und Bootstraphintergrundfarben
// (eigentlich Bootstrap Hintergrundfarbklassen)
let colorMap =
{
    Black: 'bg-secondary',
    Red: 'bg-danger',
    Blue: 'bg-primary',
    Green: 'bg-success',
    Yellow: 'bg-warning'
}

// Basis URL für API Aufrufe
let baseURL = 'http://nowaunoweb.azurewebsites.net'


// Modalen Dialog öffnen um Namen einzugeben
$('#playerNames').modal()



// Funktion zur Überprüfung, dass alle 4 Namen unterschieldlich und nicht leer sind
// true zurückliefern wenn wir 4 unterschiedliche namen
// false falls nicht
let validatePlayerNames = function(form)
{
    let fields = $("input:text", form)
    // die values in einem array haben
    // konvertiert mein feld mit den input textfeldern
    // in ein neues feld mit den werten
    let values = fields.map(function() {
        return this.value
    }).filter(function (){
        //Herausfiltern der leeren Elemente (= Selektierne die nicht leeren elemente)
        return this.length > 0
    }).get()

    return (new Set(values)).size == fields.length
}

// nach jeder tasteneingabe im formular überprüfen ob
// 4 eindeutige spielerInnennamen vorhanden sind
$('#playerNamesForm').on('keyup', function(evt) {
    //console.log(evt)
    $('#playerNamesSubmit').prop('disabled',
        !validatePlayerNames(evt.currentTarget))
    // alternative variante
    //$('button', evt.currentTarget)...
})

$('#playerNamesForm').on('submit', function(evt)
{
    // Formular absenden verhindern
    //console.log(evt)
    evt.preventDefault()
    let names = $('input:text', evt.currentTarget)
                .prop('disabled', true).map(function(){
                    return this.value
                }).get()
    //console.log(names)

    let request = $.ajax({
        url: baseURL + '/api/game/start',
        method: 'POST',
        data: JSON.stringify(names),
        contentType: 'application/json',
        dataType: 'json'
    })

    // Spiel starten erfolgreich
    request.done(function(data){
        console.log(data)
        // Blenden SpielerInnen Namen Dialog aus
        $('#playerNames').modal('hide')
        // Init das Spiel
        initGame(data)
    })

   // Spiel starten fehlgeschlagen
   request.fail(function(msg)
   {
       console.log("Error in request ", msg)
       // TODO (mach ich nicht weil faul)
       // Eingabefelder wieder aktivieren
   })

}
)


    // Spielfeld initialisieren
    let initGame = function(data)
    {
        // speichere globale spiel ide
        game = data.Id
        $('#currentdirection').append("<img src='arrowDown.png' id='imgDirection'></img>")
        let $board = $('#board')

        $topCard.append(createCard(data.TopCard))


        // click event handler für Abhebebutton
        $draw.on('click', function(event){
            playable = false

            // abhebe button inaktiv schalten bis request abgeschlossen
            $draw.prop('disabled', true)
            //draw card request schicken
            let request = $.ajax({
                url: baseURL + '/api/Game/DrawCard/' + game,
                method: 'PUT'
            })
            // request fehlgeschlagen nichts machen
            request.fail(function(msg){
                playable = true

                //abhebe button wieder aktivieren
                $draw.prop('disabled', false)
                console.log('Error drawing card', msg)
            })
            // request erfolgreich
            request.done(function(data){
                //console.log('Draw card', data)

                //erzeuge spielbare karte
                let $card = createPlayableCard(data.Card, data.Player)
                // haenge karte zum spieler

                $card.css('opacity', '0');
                $card.addClass('slide-in');
                $('.deck', players[data.Player]).append($card)
                setTimeout(function() {
                    $card.removeClass('slide-in');
                    $card.css('opacity', '1');
                }, 600)

                // zum naechsten spielerIn wechseln
                updateCurrentPlayer(data.NextPlayer)
                playable = true
                $draw.prop('disabled', false) // abhebe button wieder aktivieren


            })


        })


        // alle spielerInnen initialisieren
        data.Players.forEach(function(player) {

            let $player = createPlayer(player)

            //mappen spielerInnen name zu ihrem div
            players[player.Player] = $player

            $('#Liste').append($player)
        })
        console.log(players)
        // array mit spielernamen
        playerOrder = data.Players.map(function(pl){
            return pl.Player
        })

        // auf aktive spielerIn setzen
        updateCurrentPlayer(data.NextPlayer)

        $board.removeClass('invisible')

        playable = true

    }


    let updateCurrentPlayer = function(pl, score)
    {
        // pruefen ob wir nicht auf schon aktuellen spieler wechslen
        if(pl === currentPlayer)
        {
            console.log('No update - this player already active')
            return
        }
        currentPlayer = pl
        //namen oben in nav bar setzen
        $('#currentplayer').text('Aktueller Spieler: ' + currentPlayer)

        // aktiven spielerIn erhält rahmen, beim rest entfernen
        Object.keys(players).forEach(function(name)
        {
            if(name === currentPlayer)
            {
                // bei aktiver spielerIn rahmen hinzufuegen
                players[name].addClass('testclass')
                players[name].removeClass('Schrotflinte')
            }
            else{
                // bei allen anderen rahmen entfernen
                players[name].removeClass('testclass')
                players[name].addClass('Schrotflinte')
            }
        })

        // punkte aktualisieren, wenn keine spezifiziert sind wird nichts verändert
        if(null != score){
            $('.score', players[pl]).text('Punkte: ' + score)
        }

    }

    // Erzeugen eine/n Spieler/in auf Basis der Server Antwort
    let createPlayer = function(player) {
        // Template auswählen, Text davon auslesen und jQuery Node
        // erzeugen
        let $player = $($('#tmplPlayer').text())
        // Name setzen
        $('#alertName', $player).text("Spieler: " + player.Player)
        $('#alertName', $player).append("<span class='float-right score'>" + 'Punkte: ' + player.Score)

        // Punkte anzeigen
       //$('#alertScore', $player).text("Punkte : " + player.Score)

        let $deck = $('.deck', $player)
        // jede karte erzeugen und ins Deck hängen
        player.Cards.forEach(function(card){
            let $card = createPlayableCard(card, player.Player)
            $deck.append($card)
            slideInCardsAtTheBeginning($card);
        })

        return $player
    }

    //Animationen

    let removeCard = function($card){
        $($card).addClass('fade-out');

        setTimeout(function() {
            $card.removeClass('fade-out')
        }, 1000)
    }

    let shakeCard = function($card) {
        $card.addClass('shake')
        //Klasse entfernen damit wiederholt funktioniert
        setTimeout(function() {
            $card.removeClass('shake')
        }, 3000)
    }


    let slideInCardsAtTheBeginning = function($card){
      $card.addClass('w3-spin')
      setTimeout(function() {
        $card.removeClass('w3-spin')
      }, 3000)

    }



    let createPlayableCard = function(card, player)
    {
        let $card = createCard(card)

        // click event listener fuer karte
        $card.on('click', function(event){
            // Ueberpruefen ob Karte der aktiven SpielerIn
            if(player !== currentPlayer)
            {
                shakeCard($card)
                return
            }
            if(!playable) // falls noch eine aktion im gange ist
            {
                return
            }
            playable = false
            if(colorPickerCards.includes(card.Value))
            {
                // muessen farbidalog anzeigen
                // fuer farbwahl buttons auf klick reagieren
                $('button', $colorPicker).on('click', function(event)
                {
                    // event handler entfernen
                    $('button', $colorPicker).off('click')
                    // dialog ausblenden
                    $colorPicker.modal('hide')
                    console.log(event)
                    sendCard(card, $card, $(event.currentTarget).data('color'))

                })
                $colorPicker.modal('show') // farbdialog anzuzeigen

            }
            else
            {
                // muessen keinen farbidalog anzeigen
                sendCard(card, $card)
            }
        })

        return $card
    }

    // Erzeuge ein Kartenobjekt auf Basis der Vorlage
    // liefert diese Karte als Rückgabwert
    let createCard = function(card)
    {
        // Template auswählen, Text davon auslesen und
        // jquery node erzeugen
        let pfad = CardConverterToImg(card.Value, card.Color);
        let $card = $("<img class='mr-2 img-fluid' src='" + pfad + "'> </img>");
        // Hintergrundfarbe setzen, Titel und Untertitel setzen
       // $card.addClass(colorMap[card.Color])
       // $('.card-title', $card).text(card.Value)
       // $('.card-subtitle', $card).text(card.Text)


        return $card
    }

    // sende zu spielend karte an server
    let sendCard = function(card, $card, wildColor){
        //abhebe button inaktiv schalten
        $draw.prop('disabled', true)
        console.log('Playing card', card, $card)
        // url aubauen fuer request
        let url = baseURL + '/api/Game/PlayCard/' + game + '?value=' +
                  card.Value + '&color=' + card.Color + '&wildColor='
        // falls auf wildcolor was oben steht dann anhängen
        if(wildColor)
        {
            url = url + wildColor
        }
        let request = $.ajax({
            url: url,
            method: 'PUT',
            dataType: 'json'
        })

        // request erfolgreich
        request.done(function(data){

            if(data.Score == 0){
                console.log('game ended');
                endOfGame(data.Player);
            }

            // schauen ob error in response
            if(data.hasOwnProperty('error'))
            {
                console.log('Error at playing card', data.error)
                shakeCard($card)
                $draw.prop('disabled', false)
                playable = true
                return
            }
            // wenn wir hier sind hatten wir anscheinend keinen error :)
            console.log('played card, response:', data);

            removeCard($card)


            /*
            $('img').click(function(){
              $(this).addClass('fade-out');
            });

            setTimeout(function(){ $card.remove(); }, 900);
*/


            // falls ein farbwechsel dann abgelegte kart einfärben
            if(wildColor)
            {
                card.Color = wildColor
            }
            // am ablagestapel alte karte entfernen und durch neue karte
            // ersetzen
            $topCard.empty().append(createCard(card))


             // karte aus der hand entfernen

            //spielrichtungsaenderung beruecksichtigen
            if(reverseCards.includes(card.Value))
            {
                direction = -direction

            }

            if(direction == -1){

              $('#currentdirection').replaceWith( "<div class='card-title' id='currentdirection'>Aktuelle Richtung:  <img src='arrowUp.png' id='imgDirection'></img> </div>")
            }
            else{

              $('#currentdirection').replaceWith( "<div class='card-title' id='currentdirection'>Aktuelle Richtung:  <img src='arrowDown.png' id='imgDirection'></img> </div>")
            }

            // spieler die übersprungen werden und neue karten bekommen
            if(skipCards.includes(card.Value)) {
               console.log("start")
                updatePlayerCards(getSkippedPlayer())

               //wirklich ein TODO? skipped player punkte auch aktualsieren
            }
            console.log("Played card", data)
            // hat person gewonnen?
            setTimeout(function(){ $card.remove(); }, 400);

            // falls spielerin letzte karte gespielt hat -> siegerin

/*
            if(players[currentPlayer] && players[currentPlayer].Cards && players[currentPlayer].Cards.length == 1)
            {

                console.warn('Hand is empty', );

                players[currentPlayer].addClass('bg-success')
                $('#modal-end-of-game').modal('show');

                let endOfGameList = null;
                let winner = 0;

              $('#modal-end-of-game .modal-body').append("<ul id='list-of-winners' class='list-group'></ul>");


              //dieser Teil funktioniert nicht!!
              console.log(players)

              Object.keys(players).forEach(function(name) {


                if (currentPlayer === name) {

                  $("#list-of-winners").append("<li class='list-group-item'><h2> <i class='fas fa-trophy pr-4 winner-dance-left'></i><span id='winner-text'>" + name + "</span><i class='fas fa-trophy pl-4 winner-dance-right'></h2></i></li>");
                } else {
                  $("#list-of-winners").append("<li class='list-group-item'><h2> " + name + ", Score: " + $points.text() + "</h2></li>");

                }

              })

            }
*/
            // naechste spielerin weiterschalten
            updateCurrentPlayer(data.Player, data.Score)

            $draw.prop('disabled', false)
            playable = true
        })


        // request schlug fehl
        request.fail(function(msg){
            console.log('Server denied card', card, msg)
            shakeCard($card)

            $draw.prop('disabled', false)
            playable = true
        })

    }

    function endOfGame(winner) {

      $('#modal-end-of-game').modal('show');

      $('#modal-end-of-game .modal-body').append("<h2><i class='fas fa-trophy pr-4'></i><span id='winner-text'>" + winner + "</span><i class='fas fa-trophy pl-4'></i></h2>");
    }

    function restartGame() {
      location.reload();
    }


    // liefere uns namen der uebersprungen person
    let getSkippedPlayer = function() {
        // liefert index von aktueller spielerIn
        let currentIndex = playerOrder.indexOf(currentPlayer)
        let newIndex = (currentIndex + direction) % playerOrder.length
        if(newIndex < 0)
        {
            newIndex = newIndex + playerOrder.length
        }
        return playerOrder[newIndex]
    }
    // alle karten einer spielerIn abfragen
    let updatePlayerCards = function(player) {
        let url = baseURL + '/api/game/getCards/' + game + '?playerName=' + player
        console.log(url)
        let request = $.ajax(
            {
                url: url,
                method: 'GET',
                dataType: 'json'
            }
        )
        request.done(function(data) {
            // html bereich auswählen in dem karten sind
            let $deck = $('.deck', players[player])
            // spielkarten entfernen
            $deck.empty()
            // alle spielkarten erzeugen
            data.Cards.forEach(function(card) {
                let $card = createPlayableCard(card, player)
                $deck.append($card)
            })
        })
        request.fail(function(msg)
        {
            console.error(msg)
        })

    }


    let CardConverterToImg = function(cardValue, cardColor) {
        let imgPath = "cards/";
        if ("Blue" == cardColor) {
          if (13 == cardValue) {
            imgPath = imgPath + "Blue13.png";
          } else if (14 == cardValue) {
            imgPath = imgPath + "wild_b.png";
          } else {
            imgPath = imgPath + "Blue";
          }
        } else if ("Green" == cardColor) {
          if (13 == cardValue) {
            imgPath = imgPath + "Green13.png";
          } else if (14 == cardValue) {
            imgPath = imgPath + "wild_v.png";
          } else {
            imgPath = imgPath + "Green";
          }
        } else if ("Yellow" == cardColor) {
          if (13 == cardValue) {
            imgPath = imgPath + "Yellow13.png";
          } else if (14 == cardValue) {
            imgPath = imgPath + "wild_j.png";
          } else {
            imgPath = imgPath + "Yellow";
          }
        } else if ("Red" == cardColor) {
          if (13 == cardValue) {
            imgPath = imgPath + "Red13.png";
          } else if (14 == cardValue) {
            imgPath = imgPath + "wild_r.png";
          } else {
            imgPath = imgPath + "Red";
          }
        } else if ("Black" == cardColor) {
          if (13 == cardValue) {
            imgPath = imgPath + "Black13.png";
          }
          if (14 == cardValue) {
            imgPath = imgPath + "Black14.png";
          }
        }

        if (0 == cardValue) {
          imgPath = imgPath + "0.png";
        } else if (1 == cardValue) {
          imgPath = imgPath + "1.png";
        } else if (2 == cardValue) {
          imgPath = imgPath + "2.png";
        } else if (3 == cardValue) {
          imgPath = imgPath + "3.png";
        } else if (4 == cardValue) {
          imgPath = imgPath + "4.png";
        } else if (5 == cardValue) {
          imgPath = imgPath + "5.png";
        } else if (6 == cardValue) {
          imgPath = imgPath + "6.png";
        } else if (7 == cardValue) {
          imgPath = imgPath + "7.png";
        } else if (8 == cardValue) {
          imgPath = imgPath + "8.png";
        } else if (9 == cardValue) {
          imgPath = imgPath + "9.png";
        } else if (10 == cardValue) {
          imgPath = imgPath + "10.png";
        } else if (11 == cardValue) {
          imgPath = imgPath + "11.png";
        } else if (12 == cardValue) {
          imgPath = imgPath + "12.png";
        }
        return imgPath;
      }

})
