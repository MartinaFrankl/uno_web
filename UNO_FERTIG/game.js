// Basis URL für API Aufrufe
let baseURL = 'http://nowaunoweb.azurewebsites.net'

let players;
let currentTopcard;
let currentPlayer;
let currentValue;
let gameID;
let nameArray;

$(document).ready(function() {
  $('#modal-names').modal('show');
  $("#modal-end-of-game").modal('hide');
  $("#modal-new-color").modal('hide');
});


//registrieren der click handler
$("#button-start-game").click(createPlayers);
$("#button-change-color").click(chooseNewColor);
$("#stack img").click(drawCard);
$("#button-restart-game").click(restartGame); 

$("#player-handcards").on("click", "img", function() {
  for (let index = 0; index < players.length; index++) {
    if ("handcards-player-" + (index + 1) == $(this).closest("ul").attr("id") && currentPlayer == players[index].Player) {
      currentValue = $(this).data("value");
      currentColor = $(this).data("color");

      if ("Black" == currentColor) {
        $("#modal-new-color").modal('show');
      } else {
        //wenns keine schwarze is, einfach ohne wild-wert zurück geben
        playCard(currentValue, currentColor, "");
      }

      //console.log($(this).data("value"), $(this).data("color"));
      return;
    }
  }
  console.log("Player wanted to play another players cards", $(this).closest("ul").attr("id"));
  console.log("Next Player", currentPlayer);
  console.log("Current Player", players[0].Player);

});

function chooseNewColor() {
  let newColor = $("#modal-new-color").find("option:selected").attr("value");
  console.log("Selected: ", $("#modal-new-color select"));
  console.log("New Color = ", newColor);
  $("#modal-new-color").modal('hide');
  playCard(currentValue, currentColor, newColor);
}

function showTopCard() {
  let showCard = convertCardObjectToImgPath(currentTopcard.Value, currentTopcard.Color);
  $("<img class='mr-2 img-fluid' src='" + showCard + "'> </img>").appendTo('#topcard');
}

function showHandCards() {

  $(".card-custom").removeClass("current-player");

  for (let playerIndex = 0; playerIndex < players.length; playerIndex++) {
    if (0 == playerIndex && 1 > $('#handcards-player-1').children().length) {
      for (let handCardIndex = 0; handCardIndex < players[playerIndex].Cards.length; handCardIndex++) {
        let handCardToAppend = convertCardObjectToImgPath(players[playerIndex].Cards[handCardIndex].Value, players[playerIndex].Cards[handCardIndex].Color);

        let liItem = $("<li class='list-inline-item'> </li>").appendTo('#handcards-player-1');
        $("<img class='mr-2 img-fluid' src='" + handCardToAppend + "'> </img>").data("value", players[playerIndex].Cards[handCardIndex].Value).data("color", players[playerIndex].Cards[handCardIndex].Color).appendTo(liItem);
      }
    } else if (1 == playerIndex && 1 > $('#handcards-player-2').children().length) {
      for (let handCardIndex = 0; handCardIndex < players[playerIndex].Cards.length; handCardIndex++) {
        let handCardToAppend = convertCardObjectToImgPath(players[playerIndex].Cards[handCardIndex].Value, players[playerIndex].Cards[handCardIndex].Color);

        let liItem = $("<li class='list-inline-item'> </li>").appendTo('#handcards-player-2');
        $("<img class='mr-2 img-fluid' src='" + handCardToAppend + "'> </img>").data("value", players[playerIndex].Cards[handCardIndex].Value).data("color", players[playerIndex].Cards[handCardIndex].Color).appendTo(liItem);
      }
    } else if (2 == playerIndex && 1 > $('#handcards-player-3').children().length) {
      for (let handCardIndex = 0; handCardIndex < players[playerIndex].Cards.length; handCardIndex++) {
        let handCardToAppend = convertCardObjectToImgPath(players[playerIndex].Cards[handCardIndex].Value, players[playerIndex].Cards[handCardIndex].Color);

        let liItem = $("<li class='list-inline-item'> </li>").appendTo('#handcards-player-3');
        $("<img class='mr-2 img-fluid' src='" + handCardToAppend + "'> </img>").data("value", players[playerIndex].Cards[handCardIndex].Value).data("color", players[playerIndex].Cards[handCardIndex].Color).appendTo(liItem);
      }
    } else if (3 == playerIndex && 1 > $('#handcards-player-4').children().length) {
      for (let handCardIndex = 0; handCardIndex < players[playerIndex].Cards.length; handCardIndex++) {
        let handCardToAppend = convertCardObjectToImgPath(players[playerIndex].Cards[handCardIndex].Value, players[playerIndex].Cards[handCardIndex].Color);

        let liItem = $("<li class='list-inline-item'> </li>").appendTo('#handcards-player-4');
        $("<img class='mr-2 img-fluid' src='" + handCardToAppend + "'> </img>").data("value", players[playerIndex].Cards[handCardIndex].Value).data("color", players[playerIndex].Cards[handCardIndex].Color).appendTo(liItem);
      }
    }

    if (currentPlayer == players[playerIndex].Player) {
      let player = '#player-' + (playerIndex + 1)
      $(player).closest(".card-custom").addClass("current-player");
    }
  }
}

function reloadTopCard(value, color) {
  currentTopcard = null;
  currentTopcard = {
    Value: value,
    Color: color,
  };

  $('#topcard img').remove();
  showTopCard();
}

function reloadHandCard(index) {
  let playerHand = '#handcards-player-' + (index + 1);
  let li = playerHand + ' li';
  $(li).remove();

  if (0 <= index && 3 >= index) {
    if (1 > $(playerHand).children().length) {
      for (let handCardIndex = 0; handCardIndex < players[index].Cards.length; handCardIndex++) {
        let handCardToAppend = convertCardObjectToImgPath(players[index].Cards[handCardIndex].Value, players[index].Cards[handCardIndex].Color);

        let liItem = $("<li class='list-inline-item'> </li>").appendTo(playerHand);
        $("<img class='mr-2 img-fluid' src='" + handCardToAppend + "'> </img>").data("value", players[index].Cards[handCardIndex].Value).data("color", players[index].Cards[handCardIndex].Color).appendTo(liItem);
      }
    }
  }
  console.log('reloadHandCard', index, playerHand, li, players[index]);
}

function currentPlayerChanged(playerName) {
  $(".card-custom").removeClass("current-player");

  for (let index = 0; index < players.length; index++) {
    let player = '#player-' + (index + 1)
    if (playerName == players[index].Player) {
      $(player).closest(".card-custom").addClass("current-player");
    }

  }
  currentPlayer = playerName;
}

function createPlayers() {
  let inputFields = $("#modal-names input:text");

  nameArray = inputFields.map(function() {
    return this.value;
  }).get();

  gameStart(nameArray);

  $('#modal-names').modal('hide');
}

function gameStart(namesArray) {

  let request = $.ajax({
    url: baseURL + "/api/Game/Start", // WOHIN?da wollen wir posts hinschicken!! könnte CASE SENSITIVE sein!!!wird sich nicht ändern, kann man in variable packen --> baseURL
    method: 'POST', //WAS FÜR EINE METHODE? was wollen wir machen?? NICHT GET!!
    data: JSON.stringify(namesArray),
    contentType: 'application/json', //da gibts einen defaultwert, der muss ünberschrieben werden! siehe https://api.jquery.com/jQuery.ajax/
    dataType: 'json' //was wir in der Arntwort zurückerwarten, da gibts einen defaultwert, der muss ünberschrieben werden!
  });
  request.done(function(data) { //wenn gültige antwort: ergebnis von request in object

    gameID = data.Id;
    players = data.Players;
    currentTopcard = data.TopCard;
    currentPlayerChanged(data.NextPlayer);

    console.log("Game ID = ", gameID);
    console.log("Current Player = ", currentPlayer);
    console.log("Respone: ", data);

    for (let index = 0; index < players.length; index++) {
      let player = '#name-player-' + (index + 1);
      $(player).text(players[index].Player);
      $(player).append("<span class='float-right score'>" + 'Punkte: ' + players[index].Score + '</span>');
    }

    showTopCard();
    showHandCards();
  });
  request.fail(function(msg) { //wenn keine gültige antwort: callback falls was schiefgeht(keine gültoge karte, oder technischer fehler, o.ä.)
    gameID = 0;
    console.log("Error in request: ", msg, request);
  });

}

function getTopCard() {
  let request = $.ajax({
    url: baseURL + "/api/Game/TopCard/" + gameID,
    method: 'GET',
    dataType: 'json',
  });
  request.done(function(data) {
    console.log("Respone: TopCard ", data);

  });
  request.fail(function(msg) {
    console.log("Error in request: ", msg, request);
  });
}

function playCard(value, color, wildColor) {
  // /api/Game/PlayCard/{id}?value={value}&color={color}&wildColor={wildColor}
  //              {id}?value="3"&color="Blue"&wildColor=''
  //              {id}?value="Draw4"&color="Black"&wildColor="Blue"

  let request = $.ajax({
    url: baseURL + "/api/Game/PlayCard/" + gameID + "?value=" + value + "&color=" + color + "&wildColor=" + wildColor,
    method: 'PUT',
    contentType: 'application/json',
    dataType: 'json'
  });
  request.done(function(data) {
    console.log("Respone: playCard ", data);


    if ("WrongColor" == data.error || "Draw4NotAllowed" == data.error) {
      //  Try again

    } else {

      if (0 == data.Cards.length) {
        endOfGame();
      }
      if ("Black" == color) {
        reloadTopCard(value, wildColor);
      } else {
        reloadTopCard(value, color);
      }

      for (let index = 0; index < players.length; index++) {
        getHandOfPlayer(players[index].Player)
      }

      currentPlayerChanged(data.Player);

    }

  });
  request.fail(function(msg) {
    console.log("Error in request, card eventually not matched: ", msg, request);
  });
}

function drawCard() {
  let request = $.ajax({
    url: baseURL + "/api/Game/DrawCard/" + gameID,
    method: 'PUT',
    contentType: 'application/json',
    dataType: 'json'
  });
  request.done(function(data) {
    console.log("Respone: drawCard ", data);


    getHandOfPlayer(currentPlayer);
    currentPlayerChanged(data.NextPlayer);
  });
  request.fail(function(msg) {
    console.log("Error in request: ", msg, request);
  });

}

// Game/DrawCard/{id}
function getHandOfPlayer(playerName) {
  let request = $.ajax({
    url: baseURL + "/api/Game/GetCards/" + gameID + "?playerName=" + playerName,
    method: 'GET',
    dataType: 'json',
  });
  request.done(function(data) {
    console.log("Respone: getHandOfPlayer ", data);

    $('.score').remove();

    for (let index = 0; index < players.length; index++) {
      if (players[index].Player == playerName) {

        players[index].Score = data.Score;


        if (players[index].Cards.length != data.Cards.length) {
          players[index].Cards = data.Cards;
          reloadHandCard(index);
        } else {
          players[index].Cards = data.Cards;
        }
      }
      let playerScore = '#name-player-' + (index + 1);
      $(playerScore).append("<span class='float-right score'>" + 'Punkte: ' + players[index].Score + '</span>');
    }

  });
  request.fail(function(msg) {
    console.log("Error in request: ", msg, request);
  });
}

function endOfGame() {

  $('#modal-end-of-game').modal('show');

  let endOfGameList = null;
  let winner = 0;

  $('#modal-end-of-game .modal-body').append("<ul id='list-of-winners' class='list-group'></ul>");

  for (let playerIndex = 0; playerIndex < 4; playerIndex++) {
    console.log("Player: ", players[playerIndex].Player);
    if (currentPlayer == players[playerIndex].Player) {
      /*TODO: Animation funzt noch nicht für winner!!!*/
      $("#list-of-winners").append("<li class='list-group-item'><h2> <i class='fas fa-trophy pr-4 winner-dance-left'></i><span id='winner-text'>" + players[playerIndex].Player + "</span><i class='fas fa-trophy pl-4 winner-dance-right'></h2></i></li>");
    } else {
      $("#list-of-winners").append("<li class='list-group-item'><h2> " + players[playerIndex].Player + ", Score: " + players[playerIndex].Score + "</h2></li>");

    }
  }
}


function restartGame() {
  location.reload();
}

// Verwandelt eine Karte vom Server in den richtigen Pfad des Bildes zur Anzeige
function convertCardObjectToImgPath(cardValue, cardColor) {
  let imgPath = "cards/";
  if ("Blue" == cardColor) {
    if (13 == cardValue) {
      imgPath = imgPath + "wild4_b.svg";
    } else if (14 == cardValue) {
      imgPath = imgPath + "wild_b.svg";
    } else {
      imgPath = imgPath + "b";
    }
  } else if ("Green" == cardColor) {
    if (13 == cardValue) {
      imgPath = imgPath + "wild4_v.svg";
    } else if (14 == cardValue) {
      imgPath = imgPath + "wild_v.svg";
    } else {
      imgPath = imgPath + "v";
    }
  } else if ("Yellow" == cardColor) {
    if (13 == cardValue) {
      imgPath = imgPath + "wild4_j.svg";
    } else if (14 == cardValue) {
      imgPath = imgPath + "wild_j.svg";
    } else {
      imgPath = imgPath + "j";
    }
  } else if ("Red" == cardColor) {
    if (13 == cardValue) {
      imgPath = imgPath + "wild4_r.svg";
    } else if (14 == cardValue) {
      imgPath = imgPath + "wild_r.svg";
    } else {
      imgPath = imgPath + "r";
    }
  } else if ("Black" == cardColor) {
    if (13 == cardValue) {
      imgPath = imgPath + "wild4.svg";
    }
    if (14 == cardValue) {
      imgPath = imgPath + "wild.svg";
    }
  }

  if (0 == cardValue) {
    imgPath = imgPath + "0.svg";
  } else if (1 == cardValue) {
    imgPath = imgPath + "1.svg";
  } else if (2 == cardValue) {
    imgPath = imgPath + "2.svg";
  } else if (3 == cardValue) {
    imgPath = imgPath + "3.svg";
  } else if (4 == cardValue) {
    imgPath = imgPath + "4.svg";
  } else if (5 == cardValue) {
    imgPath = imgPath + "5.svg";
  } else if (6 == cardValue) {
    imgPath = imgPath + "6.svg";
  } else if (7 == cardValue) {
    imgPath = imgPath + "7.svg";
  } else if (8 == cardValue) {
    imgPath = imgPath + "8.svg";
  } else if (9 == cardValue) {
    imgPath = imgPath + "9.svg";
  } else if (10 == cardValue) {
    imgPath = imgPath + "draw2.svg";
  } else if (11 == cardValue) {
    imgPath = imgPath + "skip.svg";
  } else if (12 == cardValue) {
    imgPath = imgPath + "reverse.svg";
  }
  return imgPath;
}
